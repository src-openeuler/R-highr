%global packname highr
%global packver  0.9
%global rlibdir  %{_datadir}/R/library

# Needs R-knitr, so bootstrap this
%bcond_without bootstrap

Name:             R-%{packname}
Version:          0.9
Release:          1
Summary:          Syntax Highlighting for R Source Code

License:          GPLv2+
URL:              https://CRAN.R-project.org/package=%{packname}
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{packver}.tar.gz

BuildArch:        noarch
BuildRequires:    R-devel
BuildRequires:    tex(latex)
BuildRequires:    R-xfun >= 0.18
BuildRequires:    R-testit
%if %{without bootstrap}
BuildRequires:    R-knitr
BuildRequires:    R-markdown
%endif


%description
Provides syntax highlighting for R source code. Currently it supports LaTeX and
HTML output. Source code of other languages is supported via Andre Simon's
highlight package (<http://www.andre-simon.de>).


%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css

%check
%if %{without bootstrap}
%{_bindir}/R CMD check %{packname}
%else
_R_CHECK_FORCE_SUGGESTS_=0 %{_bindir}/R CMD check %{packname} --ignore-vignettes
%endif

%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/doc
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS.Rd
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help

%changelog
* Tue May 31 2022 misaka00251 <misaka00251@misakanet.cn> - 0.9-1
- Init package (Thanks to fedora team)
